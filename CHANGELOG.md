# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](Https://conventionalcommits.org) for commit guidelines.

<!-- changelog -->

## [v1.2.3](https://gitlab.com/***PROJECT_PATH***/compare/v1.2.2...v1.2.3) (2022-10-03)




### Improvements:

* Add :crypto to extra_applications

## [v1.2.2](https://gitlab.com/***PROJECT_PATH***/compare/v1.2.1...v1.2.2) (2022-09-27)




### Improvements:

* Update Bitwise use, libs

## [v1.2.1](https://gitlab.com/***PROJECT_PATH***/compare/v1.2.0...v1.2.1) (2022-09-22)




### Bug Fixes:

* correctly validate IPv4 netmasks

## [v1.2.0](https://gitlab.com/***PROJECT_PATH***/compare/v1.1.1...v1.2.0) (2022-03-01)




### Features:

* add `Address.from_tuple/1` and `Address.from_tuple!/1`

## [v1.1.1](https://gitlab.com/***PROJECT_PATH***/compare/v1.1.1...v1.1.1) (2021-12-08)



